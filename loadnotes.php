<?php
    /* সেশন চালু করে নিবো */
    session_start();
    /* ডাটাবেসের সাথে কানেকশন করে নিবো */
    include "assets/connection.php";
    // echo "<p>this is my notes</p>";
    (isset($_SESSION["userID"]))?$userID=$_SESSION["userID"]:header("location:index.php");

    $delete_query = $mysql->query("DELETE FROM `notes` WHERE `note`=''");

     /* কুয়েরিটি সঠিক ভাবে কাজ করলো কিনা সেটা আগে চেক করে নিবো যদি ভুল থাকে তাহলে নিচের ইররটি দেখাবো */ 
    if(!$delete_query){
        exit("<div class='alert alert-danger'><strong>Something went wrong in delete_query due to ".$mysql->error." please try again later.</strong></div>");
 
        /* যদি কুয়েরি সঠিকভাবে কাজ করে তাহলে সেই কুয়েরি থেকে কোন রেজাল্ট পাওয়া গেলো কিনা চেক করবো এখানে রেজাল্ট বলতে আসলে আমরা চেক করছি এই মেইলে কোন ইউজার আছে কিনা,যদি থাকে */
    }else{
        $all_notes_query = $mysql->query("SELECT * FROM `notes` WHERE `userID`='$userID' ORDER BY `time` DESC");
        if(!$all_notes_query){
            exit("<div class='alert alert-danger'><strong>Something went wrong in all_notes_query due to ".$mysql->error." please try again later.</strong></div>");
     
            /* যদি কুয়েরি সঠিকভাবে কাজ করে তাহলে সেই কুয়েরি থেকে কোন রেজাল্ট পাওয়া গেলো কিনা চেক করবো এখানে রেজাল্ট বলতে আসলে আমরা চেক করছি এই মেইলে কোন ইউজার আছে কিনা,যদি থাকে */
        }elseif($all_notes_query->num_rows>0){
            while($note = $all_notes_query->fetch_assoc()){
                $id = $note["id"];
                $text = $note["note"];
                $time = date("F d, Y h:i:s A",$note["time"]);
                echo("<div class='notelist'><div class='col-6 col-sm-3 btn btn-danger btn-lg delete' style='widht:100%'>Delete</div><div class=' noteheader' id='$id'><div class='text'>$text</div><div class='timetext'>$time</div></div></div>");
            };
        }else{
            /* যদি কোন নোট নাথাকে */
            exit("<div class='alert alert-warning'><strong>You have not created any not yet !</strong></div>");
        };
    
    };

?>
