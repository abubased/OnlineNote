<?php
/* ইউজার যখন তার মেইলে পাঠানো এক্টিভিশন লিংকে ক্লিক করবে তখন সে এই পেইজে রিডাইরেক্ট হবে,
তাহলে প্রথমেই আমরা আগে সেশন চালু করে নিবো */
session_start();
if (!isset($_GET['email']) OR !isset($_GET['token'])) {
    $_SESSION["error"] = "<div class='alert alert-danger' style='width:80%; margin:auto;'><strong><h2>Your activation link is not valid !<br>Please check your inbox to active your account</h2></strong></div>";
    header("location:../index.php");
}else{
    /* ডাটাবেসের সাথে কানেকশন করে নিবো */
    include "connection.php";
    $email = $_GET['email'];
    $token = $_GET['token'];
    $activeAccountQuery = "UPDATE `user` SET `status`='active' WHERE `email`='$email' AND `status`='$token'";
    $mysql->query($activeAccountQuery);

     /* কুয়েরিটিতে কোন ভুল আছে কিনা চেক করবো যদি ভুল থাকে তবে ইউজারকে index.php পেইজে নিয় যাবো এবং নিচের মেসেজটি প্রদর্শন করাবো */
     if($mysql->error){
        $_SESSION["error"] = "<div class='alert alert-danger'><strong>Something went wrong in signUpQuery due to ".$mysql->error." please try again later.</strong></div>";
        header("location:../index.php");
    
    };

    /* যদি ডাটাবেসের অন্ততো একটি সারির পরিবর্তন ঘটে বা যদি আমাদের কুয়েরিটি সফলভাবে ইউজারের status কে পরিবর্তন করতে পারে তাহলে ইউজারকে index.php পেইজে নিয়ে যাবো এবং সেশনের ব্যাবহার করে ইউজারকে কনফারমেশন মেসেজ দেখাবো । */
    if ($mysql->affected_rows>0) {
        $_SESSION["success"] = "<div class='alert alert-success text-center'><strong>Account activated login</strong></div>";
        header("location:../index.php");
    };
    
};
