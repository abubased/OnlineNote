<?php
/* সেশন চালু করে নিবো */
session_start();

/* ডাটাবেসের সাথে কানেকশন করে নিবো */
include "connection.php";

/* ফরমে কোন ইরর থাকলে কোন মেসেজ ্গুলি দেখাবো তা নির্ধারণ করে নিবো । */
$missingUsername = "<p><strong>Please input your username</strong></p>";
$invalidUsername = "<p><strong>Username already taken try another one</strong></p>";
$missingEmail = "<p><strong>Please enter your email</strong></p>";
$invalidEmail = "<p><strong>Please enter a valid Email</strong></p>";
$missingPassword = "<p><strong>Please enter your password</strong></p>";
$invalidPassword = "<p><strong>Password must be atlest 6 charecter of combination <strong> atlest one capital letter and a number</strong></strong></p>";
$missingPassword2 = "<p><strong>Please Confirm your password</strong></p>";
$unmatchPassword = "<P><strong>Password doesnot matched try again</strong></P>";

/* প্রথমে একটি ফাকা ভেরিয়েবল নিয়ে রাখবো পরে ইরর পেলে ইরর মেসেজ গুলী এই ভেরিয়েবলে যোগ করে দিবো */
$errors = NULL;

/* যদি ইউজার নেইম ফিল্ড খালি থাকে */
if (empty($_POST['username'])) {
    $errors .= $missingUsername;
}else {
    
    /* যদি খালি না থাকে */
    $userName = filter_var($_POST['username'],FILTER_SANITIZE_STRING);
};

/* যদি ইমেইল ফিল্ড খালি থাকে */
if (empty($_POST['email'])) {
    $errors .= $missingEmail;
}else {
    /* যদি খালি না থাকে */
    $email = filter_var($_POST['email'],FILTER_SANITIZE_STRING);

    /* যদি ইমেইলটি ভুল ফরমেটের হয় */
    if(!filter_var($email,FILTER_SANITIZE_EMAIL)){
        $errors .= $invalidEmail;
    };
};

/* যদি পাসওয়ার্ড ফিল্ড খালি থাকে */
if (empty($_POST['password'])) {
    $errors .=$missingPassword;

/* যদি খালি নাথাকে তাহলে চেক করবো পাসওয়ার্ডটি ৬ অক্ষরের কিনা এবং তার ভিতর অন্ততো একটি বড়ো হাতের অক্ষর এবং একটি সংখ্যা আছে কিনা, যদি না থাকে তাহলে ইরর দেখাবো */
}elseif(!(strlen($_POST['password'])>6 && preg_match('/[A-Z]/',$_POST['password']) && preg_match('/[0-9]/',$_POST['password'])) ){
    $errors .= $invalidPassword;
}else{

    /* যদি পাসওয়ার্ড শক্তিশালী হয় */
    $password = $_POST['password'];
};

/* কনফার্ম পাসওয়ার্ড ফিল্ড যদি খালি থাকে */
if (empty($_POST['password2'])) {
    $errors .= $missingPassword2;

/* যদি খালি নাথাকে তাহলে চেক করবো পাসওয়ার্ড এবং কনফার্ম পাসওয়ার্ডে মিল আছে কিনা */
}elseif($_POST['password'] != $_POST['password2']){
    /* , যদি মিল না থাকে */
    $errors .= $unmatchPassword;
};

/* ফরমে যদি কোন ইরর থাকে */ 
if (!empty($errors)) {
    echo "<div class='alert alert-danger'>".$errors."</div>";

    /* ফরমে যদি কোন ইরর না থাকে তাহলে ফরমের মাধ্যমে পাঠানো ডাটাগুলির মধ্যে কোন স্পেশাল  ক্যারেক্টার থাকে তা রিমুভ করে নিবো  */ 
}else{
    $userName = $mysql->real_escape_string($userName);
    $email = $mysql->real_escape_string($email);

    /* ইউজার যে পাসওয়ার্ড সেট করবে সেটি হুবহু ডাটাবেসে রাখা যাবেনা তাই পাসওয়ার্ডটিকে হ্যাশ মেথডে বিক্রিপ্ট করে নিয়ে password ভেরিয়েবলে রাখবো */
    $password = password_hash($password,PASSWORD_BCRYPT);

    /* একটি sql কুয়েরি চালাবো যেটা দেখবে যে আমাদের টেবিলে আগে থেকেই এইনামে কোন ইউজার আছে কিনা */
    $checkUserNameQuery = $mysql->query("SELECT * FROM `user` WHERE `username`='$userName'");

    /* কুয়েরিটি সঠিক ভাবে কাজ করলো কিনা সেটা আগে চেক করে নিবো যদি ভুল থাকে তাহলে নিচের ইররটি দেখাবো */ 
    if(!$checkUserNameQuery){
        exit("<div class='alert alert-danger'><strong>Something went wrong in checkUserNameQuery due to ".$mysql->error." please try again later.</strong></div>");

    /* যদি কুয়েরি সঠিকভাবে কাজ করে তাহলে সেই কুয়েরি থেকে কোন রেজাল্ট পাওয়া গেলো কিনা চেক করবো এখানে রেজাল্ট বলতে আসলে আমরা চেক করছি আগে থেকে একই নামে কোন ইউজার আছে কিনা,যদি থাকে তাহলে ইরর দেখাবো */
    }elseif($checkUserNameQuery->num_rows>0){
        exit("<div class='alert alert-danger'><strong>Username already exist</strong></div>");
    }; /* end username already have or not */ 

    /* একটি sql কুয়েরি বানিয়ে নিবো যেটা দেখবে যে আমাদের টেবিলে আগে থেকেই এইমেইলে কোন একাউন্ট রেজিস্টার করা আছে কিনা */
    $checkEmailQuery = $mysql->query("SELECT * FROM `user` WHERE `email`='$email'");

    /* কুয়েরিটি সঠিক ভাবে কাজ করলো কিনা সেটা আগে চেক করে নিবো যদি ভুল থাকে তাহলে নিচের ইররটি দেখাবো */ 
    if (!$checkEmailQuery) {
        exit("<div class='alert alert-danger'><strong>Something went wrong in checkEmailQuery due to ".$mysql->error." please try again later.</strong></div>");
    
    /* যদি কুয়েরি সঠিকভাবে কাজ করে তাহলে সেই কুয়েরি থেকে কোন রেজাল্ট পাওয়া গেলো কিনা চেক করবো এখানে রেজাল্ট বলতে আসলে আমরা চেক করবো আগে থেকে এই মেইলে কোন ইউজার রেজিস্টার আছে কিনা,যদি থাকে তাহলে ইরর দেখাবো */
    }elseif($checkEmailQuery->num_rows>0){
        exit("<div class='alert alert-danger'><strong>Already have an account belongs to this mail <a href='#loginModal' data-toggle='modal' onclick='openLogin()'>Login</a></strong></div>");

    /* যদি এইনামে কিংবা এইমেইলে কোন ইউজার আগে থেকে না থাকে */
    }else{
        /* প্রথমে একটি রেনডম এক্টিভেশন কি বানিয়ে নিবো */
        $token = bin2hex(random_bytes(16));
        /* ইউজারের  মেইলে যে এক্টিভেশন লিংক পাঠাবো, এক্টিভেশন কি এবং মেইলটি সহো সেটি বানিয়ে নিবো */
        $message = "http://localhost/OnlineNote/assets/active.php?email=".urlencode($email)."&token=".$token;

        /* ইউজারের মেইলে মেইল পাঠাবো */
        if(mail($email,"Active your account",$message,"From:jibonerasa9@gmail.com")){

            /* যদি মেইলটি সঠীক ভাবে ইউজারের মেইলে যায়, তাহলে একটি sql কুয়েরি বানিয়ে নিবো যেটি ইউজারের ডাটাগুলি বা তথ্যগুলি ডাটাবেসের একটি নির্দীষ্ট টেবিলে ঢুকাবে */
            $signUpQuery = "INSERT INTO `user`(`username`,`email`,`password`,`status`) VALUES('$userName','$email','$password','$token')";

            /* এখন কুয়েরিটি চালাবো */
            $mysql->query($signUpQuery);

            /* কুয়েরিটিতে কোন ভুল আছে কিনা চেক করবো যদি ভুল থাকে তবে নিচের মেসেজটি প্রদর্শন করাবো */
            if($mysql->error){
                exit("<div class='alert alert-danger'><strong>Something went wrong in signUpQuery due to ".$mysql->error." please try again later.</strong></div>");
            }; /* end check if have any error to insert data to user table */

            /* তারপর  ইউজারকে একটি মেসেজের মাধ্যমে জানিয়ে দিবো  */
            echo "<div class='alert alert-success'><strong>A activation link sent to this \n ".$email."\n Please check your inbox to active your account.</strong></div>";
        }; /* end to send mail */   
    }; /* end if email already have registered */ 
    
}; /* end check if any error in form */ 

