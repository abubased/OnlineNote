<?php
/* সেশন চালু করে নিবো */
session_start();

/* ফরমে কোন ইরর থাকলে কোন মেসেজ ্গুলি দেখাবো তা নির্ধারণ করে নিবো । */
$missingEmail = "<p><strong>Please enter your email</strong></p>";
$invalidEmail = "<p><strong>Please enter a valid Email</strong></p>";
$missingPassword = "<p><strong>Please enter your password</strong></p>";
$invalidPassword = "<p><strong>Password doesn't match</strong></strong></p>";
/* প্রথমে একটি ফাকা ভেরিয়েবল নিয়ে রাখবো পরে ইরর পেলে ইরর মেসেজ গুলী এই ভেরিয়েবলে যোগ করে দিবো */
$errors = NULL;

/* যদি ইমেইল ফিল্ড খালি থাকে */
if (empty($_POST["loginemail"])) {
    $errors .= $missingEmail;
}else {
    /* যদি খালি না থাকে */
    $email = filter_var($_POST['loginemail'],FILTER_SANITIZE_STRING);

    /* যদি ইমেইলটি ভুল ফরমেটের হয় */
    if(!filter_var($email,FILTER_SANITIZE_EMAIL)){
        $errors .= $invalidEmail;
    };
};
/* যদি পাসওয়ার্ড ফিল্ড খালি থাকে */
if (empty($_POST["loginpassword"])) {
    $errors .=$missingPassword;

/* যদি খালি নাথাকে */
}else{
    $password = $_POST['loginpassword'];
};

/* ফরমে যদি কোন ইরর থাকে */ 
if (!empty($errors)) {
    echo "<div class='alert alert-danger'>".$errors."</div>";

    /* ফরমে যদি কোন ইরর না থাকে */ 
}else{
    /* ডাটাবেসের সাথে কানেকশন করে নিবো */
    include "connection.php";
    /* ফরমের মাধ্যমে পাঠানো ডাটাগুলির মধ্যে কোন স্পেশাল  ক্যারেক্টার থাকে তা রিমুভ করে নিবো  */
    $email = $mysql->real_escape_string($email);
    $password = $mysql->real_escape_string($password);

    /* একটি কুয়েরি চালিয়ে নিবো */
    $loginQuery = $mysql->query("SELECT * FROM `user` WHERE `email`='$email' AND `status`='active'");

     /* কুয়েরিটি সঠিক ভাবে কাজ করলো কিনা সেটা আগে চেক করে নিবো যদি ভুল থাকে তাহলে নিচের ইররটি দেখাবো */ 
    if(!$loginQuery){
        exit("<div class='alert alert-danger'><strong>Something went wrong in checkUserNameQuery due to ".$mysql->error." please try again later.</strong></div>");
 
        /* যদি কুয়েরি সঠিকভাবে কাজ করে তাহলে সেই কুয়েরি থেকে কোন রেজাল্ট পাওয়া গেলো কিনা চেক করবো এখানে রেজাল্ট বলতে আসলে আমরা চেক করছি এই মেইলে কোন ইউজার আছে কিনা,যদি থাকে */
    }elseif($loginQuery->num_rows>0){

        /* কুয়েরি রেসাল্ট থেকে ডাটা তুলে এনে একটি ভেরিয়েবলে রাখবো */
        $userDetails = $loginQuery->fetch_assoc();
         
        /* এখন ডাটাবেসের পাসওয়ার্ড এর সাথে ইউজারের দেওয়া পাসওয়ার্ডটি ভেরিভাই করবো */
        $checkPassword = password_verify($password,$userDetails["password"]);

        /* যদি $checkPassword true রিটার্ন করে তাহলে ইউজারকে লগইন করাবো এবং কিছু তথ্য সেসনে রাখবো পরবর্তিতে ব্যাবহার করার জন্য */
        if($checkPassword){
            echo "success";
            $_SESSION["userID"] = $userDetails["userID"];
            $_SESSION["userName"] = $userDetails["username"];
            $_SESSION["userEmail"] = $userDetails["email"];
            if (isset($_POST["rememberme"])) {
                $authnicatorOne = bin2hex(random_bytes(16));
                $authnicatorTwo = bin2hex(random_bytes(16));
                $cookieValue = $authnicatorOne.",".$authnicatorTwo;
                $tenDays = time()+864000;
                setcookie("rememberme",$cookieValue,$tenDays,"/OnlineNote");
                $hashAuthnicatorTwo = password_hash($authnicatorTwo,PASSWORD_BCRYPT);
                $userID = $_SESSION["userID"];
                // $userID = $userDetails["userID"];
                $expireDate = date("Y-m-d H:i:s",$tenDays);
                $mysql->query("INSERT INTO `rememberme`(`userID`,`authnicatorOne`,`authnicatorTwo`,`expireDate`) VALUES('$userID','$authnicatorOne','$hashAuthnicatorTwo','$expireDate')");
                if($mysql->error){
                    exit("<div class='alert alert-danger'><strong>Something went wrong in remember me query due to ".$mysql->error." please try again later.</strong></div>");
                };
            };
        }else{
            /* পাসওয়ার্ড ভুল প্রমানিত হলে ইরর মেসেজ দেখাবো */
            exit("<div class='alert alert-danger'>".$invalidPassword."</div>");
        };
    }else{
         /* যদি কোন ইউজার নাথাকে */
         exit("<div class='alert alert-danger'><strong>User not Found !</strong></div>");
    };
}; /* end check if any error in form */ 


