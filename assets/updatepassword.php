<?php
if (!empty($_GET["token"]) AND !empty($_GET["user"])) {
    $userID = $_GET["user"];
    $token = $_GET["token"];
}elseif ($_SERVER["REQUEST_METHOD"]=="POST") {
    $missingPassword = "<p class='alert alert-danger'><strong>Please enter your password</strong></p>";
    $invalidPassword = "<p class='alert alert-danger'><strong>Password must be atlest 6 charecter of combination <strong> atlest one capital letter and a number</strong></strong></p>";
    $missingPassword2 = "<p class='alert alert-danger'><strong>Please Retype your password</strong></p>";
    $unmatchPassword = "<P class='alert alert-danger'><strong>Password doesnot matched try again</strong></P>";
    (empty($_POST["new-password"]))?exit($missingPassword):(!(strlen($_POST['new-password'])>6 && preg_match('/[A-Z]/',$_POST['new-password']) && preg_match('/[0-9]/',$_POST['new-password'])))? exit($invalidPassword) : $password =$_POST["new-password"];
    (empty($_POST["confirm-password"]))?exit($missingPassword2):$confirm_password =$_POST["confirm-password"];
    if ($password===$confirm_password) {
        include "connection.php";
        $userID = $mysql->real_escape_string($_POST["user"]);
        $token = $mysql->real_escape_string($_POST["token"]);
        $expireDate=time();
        $findToken=$mysql->query("SELECT `userID` FROM `forgotpassword` WHERE `userID`='$userID' AND `token`='$token' AND `status`='pending' AND `expireDate`>'$expireDate' LIMIT 1");
        ($mysql->error)?exit("<p class='alert alert-danger'>There is an error in findtoken query</p>"):($findToken->num_rows!==1)?exit("<p class='alert alert-danger'>Your token is invalid</p>"):$getUserID=$findToken->fetch_assoc();
        if (isset($getUserID)) {
            $userID = $getUserID["userID"];
            $password = password_hash($password,PASSWORD_BCRYPT);
            $mysql->multi_query("UPDATE `user` SET `password`='$password' WHERE `userID`='$userID';UPDATE `forgotpassword` SET `status`='active',`expireDate`='$expireDate' WHERE `userID`='$userID'");
            if($mysql->error){
                exit("<p class='alert alert-danger'>There is an error in multi_query <br> $mysql->error query</p>");
            };
            exit("<p class='alert alert-success'>Password updated successfuly please <a href='../index.php#loginModal' onload='openLogin()'>login</a></p>");
        };
    }else{
        exit("<p class='alert alert-danger'>Password doesnot match</p>");
    };
    // header("location:updatepassword.php?token=".$token."&user=".$userID);
};
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Online Notes</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../styling.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Arvo" rel="stylesheet" type="text/css">
</head>
<body>
<?php if(isset($userID) AND isset($token)){
    ?>
    <form method="post" id="resetpasswordform">
        <div class="modal show" id="resetpassword" aria-labelledby="myModalLabel" style="display: block; padding-right: 17px;" aria-modal="true" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="myModalLabel">
                        Update your password:
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="resetpasswordmessage"></div>
                        <div class="form-group">
                        <label for="new-password" class="sr-only">Email:</label>
                        <input class="form-control" type="password" name="new-password" id="new-password" placeholder="New Password" maxlength="30">
                        </div>
                        <div class="form-group">
                        <label for="confirm-password" class="sr-only">Password</label>
                        <input class="form-control" type="password" name="confirm-password" id="confirm-password" placeholder="Retype Password" maxlength="30">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="user" value="<?= $userID ?>">
                        <input type="hidden" name="token" value="<?= $token ?>">
                        <input class="btn green" name="update" type="submit" value="Update">
                        <button type="reset" class="btn btn-default">
                        Cancel
                        </button>
                        <a href="../index.php" type="button" class="btn btn-default pull-left">
                        Register
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php
}else{
    ?>
    <div class="alert alert-danger" style="
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 10%;
"><p><strong>Invalid link</strong> try again</p></div>
    <?php
};?>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <script src="../js/jquery-3.5.1.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="../js/bootstrap.bundle.min.js"></script>
  <script src="../index.js"></script>
  <!-- <div class="modal-backdrop show"></div> -->
</body>
</html>
