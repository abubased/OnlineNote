<?php
    /* সেশন চালু করে নিবো */
    session_start();
    /* ডাটাবেসের সাথে কানেকশন করে নিবো */
    include "connection.php";

    $userID = $_SESSION["userID"];
    $current_password =  $_POST['current_password'];
    $password = $_POST["password"];
    $confirm_password = $_POST["confirm_password"];

    if(!(strlen($password)>6 && preg_match('/[A-Z]/',$password) && preg_match('/[0-9]/',$password))){
        exit("notstrong");
    }else{
        if ($password==$confirm_password) {
            $old_password = $mysql->query("SELECT `password` FROM `user` WHERE `userID`='$userID'")->fetch_assoc();
            $old_password = $old_password["password"];
            if (password_verify($current_password,$old_password)) {
                $password = password_hash($password,PASSWORD_BCRYPT);
                $updatePasswordQuery = "UPDATE `user` SET `password`='$password' WHERE `userID`='$userID'";
                $updatePasswordQuery = $mysql->query($updatePasswordQuery);
                if(!$updatePasswordQuery){
                    echo "<div class='alert alert-warning'><p>Sorry! password updated failed due to an error in updatePasswordQuery to update your password.</p></div>";
                }else{
                    echo "<div class='alert alert-success'><p><strong>Password updated successfully</strong></p></div>";
                };
            }else{
                exit("wrong");
            exit;
            }
        }else{
            exit("notmatch");
        };
    };

?>
