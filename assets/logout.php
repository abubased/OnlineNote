<?php
/* সেশন চালু করে নিবো */
session_start();
if (isset($_SESSION["userID"]) AND $_GET["logout"]==1) {
    session_destroy();
    setcookie("rememberme",NULL,time()-86400,"/OnlineNote");
    header("location:../index.php");
};
