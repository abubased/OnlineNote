<?php
    /* সেশন চালু করে নিবো */
    session_start();
    /* ডাটাবেসের সাথে কানেকশন করে নিবো */
    include "connection.php";

    $userID = $_SESSION["userID"];
    $email = filter_var($_POST['email'],FILTER_SANITIZE_STRING);

    /* যদি ইমেইলটি ভুল ফরমেটের হয় */
    if(!filter_var($email,FILTER_SANITIZE_EMAIL)){
        echo "<div class='alert alert-warning'><p><strong>Please enter a valid Email</strong></p></div>";
    };
    $email = $mysql->real_escape_string($email);
    $updateEmailQuery = "UPDATE `user` SET `email`='$email' WHERE `userID`='$userID'";
    
    $mysql->query($updateEmailQuery);
    if($mysql->error){
        echo "<div class='alert alert-warning'><p>Sorry! Username update failed due to an error in updateEmailQuery to update username.</p></div>";
    }else{
        echo "<div class='alert alert-success'><p>Email updated successfully.</p></div>";
    };
?>
