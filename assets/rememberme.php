<?php
if (!isset($_SESSION["userID"]) AND !empty($_COOKIE["rememberme"])) {
    list($authnicatorOne,$authnicatorTwo) = explode(",",$_COOKIE["rememberme"]);
    $check = $mysql->query("SELECT * FROM `rememberme` WHERE `authnicatorOne`='$authnicatorOne'");
    /* কুয়েরিটি সঠিক ভাবে কাজ করলো কিনা সেটা আগে চেক করে নিবো যদি ভুল থাকে তাহলে নিচের ইররটি দেখাবো */ 
    if(!$check){
        $_SESSION["error"]="<div class='alert alert-danger'><strong>Something went wrong in checkUserNameQuery due to ".$mysql->error." please try again later.</strong></div>";
        exit();

    /* যদি কুয়েরি সঠিকভাবে কাজ করে তাহলে সেই কুয়েরি থেকে কোন রেজাল্ট পাওয়া গেলো কিনা চেক করবো এখানে রেজাল্ট বলতে আসলে আমরা চেক করছি আগে থেকে একই ধরনের কোন কি আছে কিনা,যদি থাকে  */
    }elseif($check->num_rows>0){
        $rememberme = $check->fetch_assoc();
        if (password_verify($authnicatorTwo,$rememberme["authnicatorTwo"])) {
            $_SESSION["userID"] = $rememberme["userID"];
            $authnicator1 = bin2hex(random_bytes(16));
            $authnicator2 = bin2hex(random_bytes(16));
            $cookieValue = $authnicator1.",".$authnicator2;
            $tenDays = time()+864000;
            setcookie("rememberme",$cookieValue,$tenDays,"/OnlineNote");
            $authnicator2 = password_hash($authnicator2,PASSWORD_BCRYPT);
            $userID = $_SESSION["userID"];
            $expireDate = date("Y-m-d H:i:s",$tenDays);
            $mysql->query("UPDATE `rememberme` SET `authnicatorOne`='$authnicator1',`authnicatorTwo`='$authnicator2',`expireDate`='$expireDate' WHERE `authnicatorOne`='$authnicatorOne'");
            if($mysql->error){
                $_SESSION['error']="<div class='alert alert-danger'><strong>Something went wrong in remember me query due to ".$mysql->error." please try again later.</strong></div>";
            };
        };
    }; /* end username already have or not */ 
};
