<?php
$errors=NULL;
$missingEmail = "<p><strong>Please enter your email</strong></p>";
$invalidEmail = "<p><strong>Please enter a valid Email</strong></p>";
if (empty($_POST["forgotemail"])) {
    exit("<div class='alert alert-warning'><p>".$missingEmail."</p></div>");
}else {
    $email = filter_var($_POST["forgotemail"],FILTER_SANITIZE_EMAIL);
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        exit("<div class='alert alert-warning'><p>".$invalidEmail."</p></div>");
    }elseif (empty($errors)) {
        include "connection.php";
        $email = $mysql->real_escape_string($email);
        $findUser = $mysql->query("SELECT `userID` FROM `user` WHERE `email`='$email' LIMIT 1");

        (!$findUser)? exit("<div class='alert alert-danger'><p>There is an error in <mark>Finduser query</mark></p></div>"):($findUser->num_rows>0)?$user=$findUser->fetch_assoc():exit("<div class='alert alert-danger'><p>User not found !</p></div>");
        if(isset($user)){
            $token = bin2hex(random_bytes(16));
            $expireDate=(time()+43200);
            $userID = $user["userID"];
            $to=$email;
            $subject= "Update password";
            $message= "Please click this link to change your password \n\n";
            $message.= "http://localhost/OnlineNote/assets/updatepassword.php?token=".$token."&user=".$userID;
            $header = "From:jibonerasa9@gmail.com";
            if(mail($to,$subject,$message,$header)){
                $mysql->query("INSERT INTO `forgotpassword`(`userID`,`token`,`expireDate`) VALUES('$userID','$token','$expireDate')");
                ($mysql->error)? exit("<div class='alert alert-danger'><p>There is an error in forgotpassword query</p></div>"): exit("<div class='alert alert-success'><p>Please check your inbox in ".$email." to create new password.</p></div>");
            }else{
                exit("<div class='alert alert-danger'><p>Unable to send mail try again later</p></div>");
            };
        };
    };
};
