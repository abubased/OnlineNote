$(function() {
  /* একটি ভেরিয়েবল নিবো যার ভেলু আমরা যখন কোন নোট ইডিট বা আপডেট করবো সেই নোটের আইডি নাম্বার */
  let activeNote;

  /* যখন পেইজ লোড হবে তখনি ডাটাবেস থেকে সকল নোটগুলো একটি এজাক্স কলের মাধ্যমে আমাদের হোমপেইজে দেখা্বো */
  $.ajax({

    url: "loadnotes.php",
    success: function(data) {
      $("#notes").html(data);
      /* noteheadr এবং delete ক্লাসটি এজাক্স কলের মাধ্যমে পেইজে লোড করানো হয় তাই এই ক্লাসগুলিতে সরাসরি ক্লিক ইভেন্ট কাজ করবেন এই সমস্যার সমাধানে জন্য, আমরা এখানে ক্লিক ইভেন্ট চালাবো তাহলে এজাক্স কলের মাধ্যমে পুরো পেইজ লোড হবার পর ক্লাস দুটিতে ক্লিক ইভেন্ট পরবে আর তখন কোন সমস্যা ছাড়াই আমাদের ইভেন্ট কাজ করবে */
      $(".noteheader").click(editnote);
      $(".delete").click(deleteNote);
    }
  });

/* ইউজার যখন addNote বাটনে ক্লিক করবে ঠিক তখনি এজাক্স এর মাধ্যমে আমরা একটি খালি নোট ক্রিয়েট করে তা ডাটাবেজে সংরক্ষন করবো এবং সেই নোটের আইডি নাম্বারটি নিয়ে activeNote নোটে রাখবো */
  $("#addNote").click(function() {
    $.ajax({
      url: "assets/createnote.php",
      success: function(data) {
        if (data == "error") {
          $("#alertMsg").html(
            "<div class='alert alert-warning'><p>There is an error in ajax call to load notes</p></div>"
          );
        } else {
          activeNote = data;
          console.log(activeNote);
        }
      },
      error: function() {
        $("#alertMsg").html(
          "<div class='alert alert-warning'><p>There is an error in ajax call to create note</p></div>"
        );
      }
    });
    showHide(["#addNote", "#notes", "#edit"], ["#notePad", "#allNotes"]);
    $("textarea").val("").focus().keyup(function () {
        $.ajax({
            url:"assets/updatenote.php",
            type:"POST",
            data:{note:$("textarea").val(),note_id:activeNote},
            success:function (data) {
    
            },
            error:function () {
                $("#alertMsg").html("<div class='alert alert-warning'><p>There is an error in ajax call to Update note</p></div>");
            }
        });
    });
  });

  /* ইউজার যখন allNotes বাটনে ক্লিক করবে তখন আবারো একটি এজাক্স কলের মাধ্যমে আমরা ডাটাবেজে সংরক্ষিত নোট গুলো লোড করাবো এবং ফাকা নোটগুলি ডিলেট করবো */
  $("#allNotes").click(function() {
    $.ajax({
      url: "loadnotes.php",
      success: function(data) {
        $("#notes").html(data);
        $(".noteheader").click(editnote);
        $(".delete").click(deleteNote);
      },
      error: function() {
        $("#notes").html(
          "<div class='alert alert-warning'><p>There is an error in ajax call to load notes</p></div>"
        );
      }
    });
    showHide(["#notePad", "#allNotes"], ["#addNote", "#notes", "#edit"]);
  });

  /* ইউজার যখন edit বাটনে ক্লিক করবে তখন edit, বাটনগুলি হাইড করে done,delete বাটনগুলি সো করাবো */
  $("#edit").click(function() {
    showHide(["#edit"], ["#done",".delete"]);
    $(".noteheader").addClass("col-6 col-sm-8");
  });

  /* ইউজার যখন done বাটনে ক্লিক করবে তখন  done,delete বাটনগুলি হাইড করে edit বাটনগুলি সো করাবো */
  $("#done").click(function() {
    showHide(["#done",".delete"],["#edit"]);
    $(".noteheader").removeClass("col-6 col-sm-8");
  });


  /* একটি ফাংশন ক্রিয়েট করে নিবো যার কাজ হবে একসাথে একাধিক ইলিমেন্ট কে হাইড এবং সো করানো */
  function showHide(hideList, showList) {
    for (let index = 0; index < hideList.length; index++) {
      let element = hideList[index];
      $(element).hide();
    }
    for (let index = 0; index < showList.length; index++) {
      let element = showList[index];
      $(element).show();
    }
  };

    /* ইউজার যখন নোটগুলির যেকোন নোটে ক্লিক করবে তখন সেই নোটের আইডি নাম্বার ব্যাবহার করে ইউজারকে উক্ত নোটটি ইডিট করার সুযোগ করে দিতে এবং ইউজার সেই নোটে নতুন কিছু টাইপ করার সাথে সাথে এজাক্স কলের মাধ্যমে উক্ত নোটটি আপডেট করে নেবার জন্য একটি ফাংশন ক্রিয়েট করে নিবো */
  function editnote() {
    activeNote = $(this).attr("id");
      let content = $(this).find(".text").text();
      showHide(["#addNote", "#notes", "#edit","#done"], ["#notePad", "#allNotes"]);
      $("textarea").val(content).focus().keyup(function () {
        $.ajax({
            url:"assets/updatenote.php",
            type:"POST",
            data:{note:$("textarea").val(),note_id:activeNote},
            success:function (data) {
    
            },
            error:function () {
                $("#alertMsg").html("<div class='alert alert-warning'><p>There is an error in ajax call to Update note</p></div>");
            }
        });
    });


  };

  /* ইউজার যখন delete বাটনে ক্লিক করবে তখন তখন সেই নোটের আইডি নাম্বার ব্যাবহার করে উক্ত নোটটি ডিলেট করে নোটলিষ্ট থেকে সেই নোটটি সরিয়ে ফেলতে একটি ফাংশন ক্রিয়েট করে নিবো */
  function deleteNote() {
    let deleteButton = $(this);
    $.ajax({
        url: "assets/deletenote.php",
        type:"POST",
        data:{note_id:deleteButton.next().attr("id")},
        success: function(data) {
          $("#alertMsg").html(data);
          setTimeout(() => {
              $("#alertMsg").empty();
          }, 1000);
          deleteButton.parent().remove();
        },
        error: function() {
          $("#notes").html(
            "<div class='alert alert-warning'><p>There is an error in ajax call to load notes</p></div>"
          );
        }
      });
  };
});
