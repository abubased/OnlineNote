$(document).ready(()=> {
    /* সাইনআপ ফরম সাবমিট করলে একটি ফাংশন কল হবে । */
    $("#signupform").submit((e)=>{
        /* ফরম সাবমিট বন্ধ করবো */
        e.preventDefault();
        /* পোষ্ট মেথডের মাধ্যমে যেসকল ডাটা যাচ্ছে সেগুলি এরে আকারে সিরিয়ালাইজ করে postData ভেরিয়েবলে রাখবো । */
        let postData = $("#signupform").serializeArray();
        console.log(postData);
        $.ajax({
            /* এজাক্স কলের মাধ্যমে ডাটাগুলি signup.php ফাইলে সেন্ড করবো । */
            url:"assets/signup.php",
            /* ডাটাগুলি POST মেথডের মাধ্যমে পাঠাবো */
            type:"POST",
            /* কোন ডাটাগুলি পাঠাবো তা নির্ধারণ করে দিবো */
            data: postData,
            /* যদি সফল হয় তাহলে একটি ফাংশন কল করবো */
            success: (data)=> {
                /* signup.php ফাইল থেকে আসা ডাটাগুলিকে #signupmessage ডিভের ভিতরে দেখাবো */
                $("#signupmessage").html(data);
                setTimeout(() => {
                    $("#signupmessage").empty(); 
                }, 1000*30);
            },
            /* যদি সফল না হয় তাহলে #signupmessage ডিভের ভিতরে একটি মেসেজ দেখাবো */
            error: ()=> {
                $("#signupmessage").html("<div class='alert alert-danger'><p>There is some error please try again later</p></div>");
            }
        });
    });
    /* লগইন ফরম সাবমিট করলে একটি ফাংশন কল হবে । */
    $("#loginform").submit((e)=>{
        /* ফরম সাবমিট বন্ধ করবো */
        e.preventDefault();
        /* পোষ্ট মেথডের মাধ্যমে যেসকল ডাটা যাচ্ছে সেগুলি এরে আকারে সিরিয়ালাইজ করে postData ভেরিয়েবলে রাখবো । */
        let postData = $("#loginform").serializeArray();
        console.log(postData);
        $.ajax({
            /* এজাক্স কলের মাধ্যমে ডাটাগুলি login.php ফাইলে সেন্ড করবো । */
            url:"assets/login.php",
            /* ডাটাগুলি POST মেথডের মাধ্যমে পাঠাবো */
            type:"POST",
            /* কোন ডাটাগুলি পাঠাবো তা নির্ধারণ করে দিবো */
            data: postData,
            /* যদি সফল হয় তাহলে একটি ফাংশন কল করবো */
            success: (data)=>{
                /* login.php ফাইল থেকে আসা ডাটাগুলিকে #loginmessage ডিভের ভিতরে দেখাবো */
                if(data=="success"){
                    location="home.php";
                }else{
                    $("#loginmessage").html(data);
                    setTimeout(()=>{
                        $("#loginmessage").empty();
                    }, 8000); 
                };
            },
            /* যদি সফল না হয় তাহলে #loginmessage ডিভের ভিতরে একটি মেসেজ দেখাবো */
            error:()=>{
                $("#loginmessage").html("<div class='alert alert-danger'><p>There is some error please try again later</p></div>");
            }
        });
    });

    $("#forgotpasswordform").submit((e)=>{
        e.preventDefault();
        let postData = $("#forgotpasswordform").serializeArray();
        console.log(postData);
        $.ajax({
            url:"assets/forgotpassword.php",
            type:"POST",
            data:postData,
            success:(data)=>{
                $("#forgotpasswordmessage").html(data);
            },
            error:()=>{
                $("#forgotpasswordmessage").html("<div class='alert alert-danger'><p>There is some error please try again later</p></div>");
                setTimeout(() => {
                    $("#forgotpasswordmessage").empty();
                }, 5000);
            }
        });
    });

    $("#resetpasswordform").submit((e)=>{
        e.preventDefault();
        let postData = $("#resetpasswordform").serializeArray();
        console.log(postData);
        $.ajax({
            url:"updatepassword.php",
            type:"POST",
            data:postData,
            success:(data)=>{
                $("#resetpasswordmessage").html(data);
            },
            error:()=>{
                $("#resetpasswordmessage").html("<div class='alert alert-danger'><p>There is some 1 error please try again later</p></div>");
                setTimeout(() => {
                    $("#resetpasswordmessage").empty();
                }, 5000);
            }
        });
    });
    

});
