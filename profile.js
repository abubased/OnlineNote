$(function () {
   $("#updateusernameform").submit(function (e) {
        e.preventDefault();
        let postData = $("#updateusernameform").serializeArray();
        $.ajax({
            url:"updateusername.php",
            type:"POST",
            data:postData,
            success:function (data) {
                $("#updateusernamemessage").html(data);
                setTimeout(() => {
                    $("#updateusernamemessage").empty();
                    $("#updateusernameform").modal("hide");
                    location.reload();
                }, 2000);
            },
            error:function () {
                $("#updateusernamemessage").html("<div class='alert alert-danger'><p>There is an error in ajax call to updateusername</p></div>");
                setTimeout(() => {
                    $("#updateusernamemessage").empty();
                }, 2000);
            }
        });
   });


   $("#updateemailform").submit(function (e) {
        e.preventDefault();
        let postData = $("#updateemailform").serializeArray();
        $.ajax({
            url:"updateemail.php",
            type:"POST",
            data:postData,
            success:function (data) {
                $("#updateemailmessage").html(data);
                setTimeout(() => {
                    $("#updateemailmessage").empty();
                    $("#updateemailform").modal("hide");
                    location.reload();
                }, 2000);
            },
            error:function () {
                $("#updateemailmessage").html("<div class='alert alert-danger'><p>There is an error in ajax call to update email</p></div>");
                setTimeout(() => {
                    $("#updateemailmessage").empty();
                }, 2000);
            }
        });
   });

   $("#updatepasswordform").submit(function (e) {
        e.preventDefault();
        let postData = $("#updatepasswordform").serializeArray();
        $.ajax({
            url:"createnewpassword.php",
            type:"POST",
            data:postData,
            success:function (data) {
                console.log(postData);
                if(data=="notstrong"){
                    $("#updatepasswordmessage").html("<div class='alert alert-warning'><p><strong>Password must be atlest 6 charecter of combination <strong> atlest one capital letter and a number</strong></strong></p></div>");
                    console.log(data);
                }else if (data=="wrong") {
                    $("#updatepasswordmessage").html("<div class='alert alert-warning'><P class='alert alert-danger'><strong>Your password is wrong. Please try again!</strong></P></div>");
                    console.log(data);
                }else if(data=="notmatch"){
                    $("#updatepasswordmessage").html("<div class='alert alert-warning'><P class='alert alert-danger'><strong>Password doesnot matched try again</strong></P></div>");
                    console.log(data);
                }else{
                    $("#updatepasswordmessage").html(data);
                    setTimeout(() => {
                        $("#updatepasswordmessage").empty();
                        $("#updatepasswordform").modal("hide");
                        location.reload();
                    }, 2000);
                };
                
            },
            error:function () {
                $("#updatepasswordmessage").html("<div class='alert alert-danger'><p>There is an error in ajax call to update password</p></div>");
                setTimeout(() => {
                    $("#updatepasswordmessage").empty();
                }, 2000);
            }
        });
   });

   
});
